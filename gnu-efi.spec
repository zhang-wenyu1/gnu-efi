%define debug_package %{nil}
Name:          gnu-efi
Version:       3.0.18
Release:       1
Summary:       Development Libraries and headers for EFI
Epoch:         1
License:       BSD
URL:           https://sourceforge.net/projects/gnu-efi
ExclusiveArch: x86_64 aarch64 loongarch64 riscv64
Source0:       https://sourceforge.net/projects/gnu-efi/files/%{name}-%{version}.tar.bz2
#stubs-32.h comes from http://www.gnu.org/software/glibc/
Source1:       stubs-32.h
Patch1:        0001-gnu-efi-add-CHAR16-for-loongarch64.patch

%ifarch riscv64
Patch2:        riscv64-fix-efibind.h-missing-duplicate-types.patch
%endif

%global efidir %(eval echo $(grep ^ID= /etc/os-release | sed  's/^ID=//'))

%ifarch x86_64
%global efiarch x86_64
%endif
%ifarch aarch64
%global efiarch aarch64
%endif
%ifarch loongarch64
%global efiarch loongarch64
%endif
%ifarch riscv64
%global efiarch riscv64
%endif

Provides:  %{name}-utils = %{epoch}:%{version}-%{release}
Obsoletes: %{name}-utils < %{epoch}:%{version}-%{release}
BuildRequires:  gcc make binutils 

%description
This package contains development headers and libraries for developing
applications that run under EFI (Extensible Firmware Interface).

%package devel
Summary:   Development Libraries and headers for EFI
Obsoletes: gnu-efi < 1:3.0.2-1
Requires: gnu-efi = %{epoch}:%{version}-%{release}

%description devel
This package contains development headers and libraries for developing
applications that run under EFI (Extensible Firmware Interface).

%prep
%autosetup -n %{name}-%{version} -p1
install -d gnuefi/gnu
install -Dp %{SOURCE1} gnuefi/gnu/


%build
unset LDFLAGS

make
make apps

%install
install -d %{buildroot}/%{_libdir}/gnuefi
install -d %{buildroot}/boot/efi/EFI/%{efidir}/%{efiarch}
make PREFIX=%{_prefix} LIBDIR=%{_libdir} INSTALLROOT=%{buildroot} install
mv %{buildroot}/%{_libdir}/*.lds %{buildroot}/%{_libdir}/*.o %{buildroot}/%{_libdir}/gnuefi
mv %{efiarch}/apps/{route80h.efi,modelist.efi} %{buildroot}/boot/efi/EFI/%{efidir}/%{efiarch}/

%files
%{_prefix}/lib*/*
%dir %attr(0700,root,root) /boot/efi/EFI/%{efidir}/*/
%attr(0700,root,root) /boot/efi/EFI/%{efidir}/*/*.efi

%files devel
%defattr(-,root,root,-)
%doc README.*
%{_includedir}/efi

%changelog
* Wed Dec 04 2024 zhang_wenyu1 <zhang_wenyu@hoperun.com> - 1:3.0.18-1
- Upgrade to version 3.0.18

* Mon Apr 08 2024 wulei <wu_lei@hoperun.com> - 1:3.0.17-7
- Remove ia32 architecture

* Wed Sep 6 2023 EastDong <xudong23@iscas.ac.cn> -  1:3.0.17-6
- fix build error in RISC-V

* Mon Aug 28 2023 chenchen <chen_aka_jan@163.com> - 1:3.0.17-5
- unset ldflags for fix build error

* Thu Jul 20 2023 zhangxiang <zhangxiang@iscas.ac.cn> - 1:3.0.17-4
- Fix ExclusiveArch for riscv64

* Wed Jul 19 2023 misaka00251 <liuxin@iscas.ac.cn> - 1:3.0.17-3
- Enable build on riscv64

* Thu Jul 6 2023 doupengda <doupengda@loongson.cn> - 1:3.0.17-2
- gnu-efi add CHAR16 for loongarch64

* Tue Jun 20 2023 chenchen <chen_aka_jan@163.com> - 1:3.0.17-1
- Upgrade to version 3.0.17

* Mon Jun 7 2021 baizhonggui <baizhonggui@huawei.com> - 3.0.8-10
- Fix building error: make[1]: gcc: No such file or directory
- Add gcc in BuildRequires

* Thu Sep 10 2020 liuweibo <liuweibo10@huawei.com> - 3.0.8-9
- Fix Source0

* Wed Mar 18 2020 likexin <likexin4@huawei.com> - 3.0.8-8
- Delete modify-cflags.patch

* Wed Mar 18 2020 likexin <likexin4@huawei.com> - 3.0.8-7
- Fix up modify-cflags.patch

* Wed Mar 18 2020 likexin <likexin4@huawei.com> - 3.0.8-6
- Add cflags -fstack-protector-strong

* Fri Mar 13 2020 zhujunhao<zhujunhao5@huawei.com> - 3.0.8-5
- Modify x86 build failed

* Wed Jan 15 2020 yuxiangyang4<yuxiangyang4@huawei.com> - 3.0.8-4
- Upgrade source code to 3.0.8

* Wed Nov 20 2019 yangjian<yangjian79@huawei.com> - 3.0.8-3
- Package init
